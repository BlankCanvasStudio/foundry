package main

import (
	"context"
	"flag"
	"time"

	log "github.com/sirupsen/logrus"
	api "gitlab.com/mergetb/tech/foundry/api"
	"google.golang.org/grpc"

	"gitlab.com/mergetb/tech/foundry/forge"
	"gitlab.com/mergetb/tech/foundry/pkg"
)

const (
	cert    = "/etc/foundry/cert.pem"
	timeout = 5 * time.Second
)

var (
	logLevel string
)

func main() {

	flag.StringVar(&logLevel, "logLevel", "info", "log level")
	flag.Parse()

	loginit()

	log.Infof("foundryc %s", foundry.Version)

	// get machine identifying info

	ids, err := forge.GetIds()
	if err != nil {
		log.WithError(err).Fatal("failed to get device ids")
	}

	err = forge.ForgePrimaryInterface()
	if err != nil {

		log.WithError(err).Warn("forge primary interface failed")

	} else {

		err = forge.RestartNetworkd()
		if err != nil {
			log.WithError(err).Fatal("failed to restart networkd")
		}

		// wait for dhcp to succeed, if not foundry will fail and systemd will start
		// us over again
		time.Sleep(5 * time.Second)

	}

	// connect to foundry

	conn, cli := connect()
	defer conn.Close()

	// forge request

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	resp, err := cli.Forge(ctx, &api.ForgeRequest{Ids: ids})
	if err != nil {
		log.WithError(err).Fatal("forge request failed")
	}

	// forge the machine

	err = forge.Forge(resp.Config)
	if err != nil {
		log.WithError(err).Fatal("forge failed")
	}

}

func connect() (*grpc.ClientConn, api.ForgeClient) {

	conn, err := grpc.Dial("foundry:47001", grpc.WithInsecure())
	if err != nil {
		log.WithError(err).Fatal("foundry connection failed")
	}
	client := api.NewForgeClient(conn)

	return conn, client

}

func loginit() {
	l, err := log.ParseLevel(logLevel)
	if err != nil {
		log.SetLevel(log.InfoLevel)
	} else {
		log.SetLevel(l)
	}
}
