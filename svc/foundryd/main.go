package main

import (
	"flag"

	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/foundry/pkg"
)

func main() {

	log.Infof("foundryd %s", foundry.Version)
	flag.Parse()

	go runFoundry()
	runManager()

}
