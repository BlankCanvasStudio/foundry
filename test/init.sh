#!/bin/bash

set -e

serverip=`rvn ip server`
ansible_dir="./ansible"

# install foundryd on server
ansible-playbook \
    -i ansible-interpreters.cfg \
    -i .rvn/ansible-hosts \
    $ansible_dir/server-deb.yml
# install and run foundryc on client
ansible-playbook \
    -i ansible-interpreters.cfg \
    -i .rvn/ansible-hosts \
    $ansible_dir/client-deb.yml \
    --extra-vars="server=$serverip"

# after client is configured, atttempt to get raven to see changes
rvn status

# verify most settings
ansible-playbook \
    -i ansible-interpreters.cfg \
    -i .rvn/ansible-hosts \
    $ansible_dir/verify-settings.yml

# test the password was configured
sshpass -p muffins ssh rygoo@$(cat .rvn/ansible-hosts | grep client | cut -d " " -f 2 | cut -d "=" -f 2) exit

# and verify public key set up as well
ssh -i keys/testkey rygoo@$(cat .rvn/ansible-hosts | grep client | cut -d " " -f 2 | cut -d "=" -f 2) exit
