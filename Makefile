prefix ?= /usr/local

.PHONY: all
all: api/spec.pb.go \
	build/foundryd\
	build/foundryc\
	build/foundry

APISRC = api/spec.pb.go api/object.go
PKGSRC = pkg/management.go pkg/store.go pkg/etcd.go pkg/config.go pkg/foundry.go $(APISRC) $(FGSRC)
FGSRC  = forge/id.go forge/forge.go forge/errors.go forge/user.go forge/interface.go forge/systemd.go forge/disk.go forge/route.go forge/cmdline.go

VERSION = $(shell git describe --always --long --dirty)
LDFLAGS = "-X gitlab.com/mergetb/tech/foundry/pkg.Version=$(VERSION)"

BLUE=\e[34m
GREEN=\e[32m
CYAN=\e[36m
NORMAL=\e[39m

build/foundryd: svc/foundryd/main.go svc/foundryd/* $(PKGSRC)
	$(QUIET) $(call go-build)


build/foundry: util/foundry/main.go util/foundry/*.go $(PKGSRC)
	$(QUIET) $(call go-build)

build/foundryc: util/foundryc/main.go util/foundry/*.go $(PKGSRC)
	$(QUIET) $(call go-build)

protoc-gen-go=.tools/protoc-gen-go
$(protoc-gen-go): | .tools
	$(QUIET) GOBIN=`pwd`/.tools go install github.com/golang/protobuf/protoc-gen-go

.PHONY: tools
tools: $(protoc-gen-go)

api/spec.pb.go: api/spec.proto
	$(protoc-build)
	$(QUIET) sed -r -i 's/json:"(.*)"/json:"\1" yaml:"\1" mapstructure:"\1"/g' api/spec.pb.go

.tools:
	$(QUIET) mkdir .tools

clean:
	$(QUIET) rm -rf build

spotless: clean
	$(QUIET) rm -rf .tools

# contianer

REGISTRY ?= docker.io
REPO ?= mergetb
TAG ?= latest
BUILD_ARGS ?= --no-cache

.PHONY: $(REGISTROY)/$(REPO)/foundry
$(REGISTRY)/$(REPO)/foundry: container/Dockerfile build/foundryd build/foundry
	$(docker-build)

.PHONY: container
container: $(REGISTRY)/$(REPO)/foundry

.PHONY: install
install: install-foundryc install-foundryd install-foundry

.PHONY: install-foundryc
install-foundryc: build/foundryc
	install -D $< $(DESTDIR)$(prefix)/bin/foundryc

.PHONY: install-foundryd
install-foundryd: build/foundryd
	install -D $< $(DESTDIR)$(prefix)/bin/foundryd

.PHONY: install-foundry
install-foundry: build/foundry
	install -D $< $(DESTDIR)$(prefix)/bin/foundry

QUIET=@
ifeq ($(V),1)
	QUIET=
endif

define build-slug
	@echo "$(BLUE)$1$(GREEN)\t $< $(CYAN)$@$(NORMAL)"
endef

define go-build
	$(call build-slug,go)
	$(QUIET) \
		go build -ldflags=${LDFLAGS} -o $@ $(dir $<)*.go
endef

define protoc-build
	$(call build-slug,protoc)
	$(QUIET) PATH=./.tools:$$PATH protoc \
		-I . \
		-I ./$(dir $@) \
		./$< \
		--go_out=plugins=grpc:.
endef

define docker-build
	$(call build-slug,docker)
	@docker build ${BUILD_ARGS} $(DOCKER_QUIET) -f $< -t $(@):$(TAG) .
	$(if ${PUSH},$(call docker-push))
endef

define docker-push
	$(call build-slug,push)
	@docker push $(@):$(TAG)
endef
