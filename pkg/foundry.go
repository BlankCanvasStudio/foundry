package foundry

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	api "gitlab.com/mergetb/tech/foundry/api"
)

var (
	// Version is the package version
	Version = "undefined"
)

func Prepare(rq *api.ForgeRequest) (*api.MachineConfig, error) {

	cfg, err := GetConfigs(rq.Ids)
	if err != nil {
		log.WithError(err).Error("get configs failed")
		return nil, err
	}

	if len(cfg) == 0 {
		return nil, fmt.Errorf("not found")
	}
	if len(cfg) > 1 {
		log.WithFields(log.Fields{
			"ids": rq.Ids,
		}).Warn("prepare: config not unique")
	}

	return cfg[0], nil

}
