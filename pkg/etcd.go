package foundry

import (
	"crypto/tls"
	"crypto/x509"
	"io/ioutil"
	"time"

	"github.com/coreos/etcd/clientv3"
	log "github.com/sirupsen/logrus"
)

//Try up to 10 times to connect to etcd
func EtcdConnect() (*clientv3.Client, error) {
	log.Trace("connecting to etcd...")
	etcd, err := EtcdClient()
	if err == nil {
		return etcd, nil
	}
	for i := 0; i < 10; i++ {
		ErrorE("error connecting to etcd", err)
		time.Sleep(1 * time.Second)
		etcd, err = EtcdClient()
		if err == nil {
			return etcd, nil
		}
	}
	return nil, ErrorE("failed to connect to etcd", err)
}

func withEtcd(f func(*clientv3.Client) error) error {

	cli, err := EtcdConnect()
	if err != nil {
		return err
	}
	defer cli.Close()

	return f(cli)

}

func EtcdClient() (*clientv3.Client, error) {

	log.Trace("creating new client...")

	cfg := ServerConfig()

	var tlsc *tls.Config
	if cfg.EtcdTls != nil {

		log.Trace("etcd tls enabled")

		log.WithFields(log.Fields{
			"cacert": cfg.EtcdTls.CA,
			"cert":   cfg.EtcdTls.Cert,
			"key":    cfg.EtcdTls.Key,
		}).Trace("tls config")

		capool := x509.NewCertPool()
		capem, err := ioutil.ReadFile(cfg.EtcdTls.CA)
		if err != nil {
			return nil, ErrorE("failed to read cacert", err)
		}
		ok := capool.AppendCertsFromPEM(capem)
		if !ok {
			return nil, ErrorF("capem is not ok", log.Fields{"ok": ok})
		}

		cert, err := tls.LoadX509KeyPair(cfg.EtcdTls.Cert, cfg.EtcdTls.Key)
		if err != nil {
			return nil, ErrorE("failed to load cert/key pair", err)
		}

		tlsc = &tls.Config{
			RootCAs:      capool,
			Certificates: []tls.Certificate{cert},
		}
	} else {
		log.Trace("etcd tls disabled")
	}

	log.WithFields(log.Fields{
		"connstr": cfg.EtcdEndpoint,
	}).Trace("etcd connection string")

	cli, err := clientv3.New(clientv3.Config{
		Endpoints:          []string{cfg.EtcdEndpoint},
		DialTimeout:        2 * time.Second,
		TLS:                tlsc,
		MaxCallSendMsgSize: MaxMessageSize,
		MaxCallRecvMsgSize: MaxMessageSize,
	})

	log.Trace("client created")
	return cli, err

}
