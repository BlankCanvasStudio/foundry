package foundry

import (
	"google.golang.org/grpc"
)

var (
	MaxMessageSize    = 4 * 1024 * 1024
	GRPCServerOptions = []grpc.ServerOption{
		grpc.MaxRecvMsgSize(MaxMessageSize),
		grpc.MaxSendMsgSize(MaxMessageSize),
	}
)
