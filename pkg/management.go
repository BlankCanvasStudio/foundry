package foundry

import (
	"context"
	"encoding/json"
	"time"

	etcd "github.com/coreos/etcd/clientv3"
	log "github.com/sirupsen/logrus"
	api "gitlab.com/mergetb/tech/foundry/api"
)

/*
func SetConfig(id string, config *api.Config) error {

	return Write(&api.MachineConfig{
		Id:     id,
		Config: config,
	})

}
*/

// SetConfigs configures foundry's datastore with machine configuration
func SetConfigs(configs []*api.MachineConfig) error {

	log.Infof("Set: %v", configs)
	var objs []Object
	for _, c := range configs {
		objs = append(objs, c)
	}

	return WriteObjects(objs)
}

// GetConfigs retrieves the configuration from the datastore
func GetConfigs(ids []string) ([]*api.MachineConfig, error) {

	log.Infof("Get: %v", ids)

	var machines []*api.MachineConfig
	for _, id := range ids {
		machines = append(machines, &api.MachineConfig{Id: id})
	}

	var objs []Object
	for _, m := range machines {
		objs = append(objs, m)
	}

	err, _ := ReadObjects(objs)

	// prune not found
	var result []*api.MachineConfig
	for _, m := range machines {
		if m.Ver > 0 {
			result = append(result, m)
		}
	}

	return result, err

}

// DeleteConfigs removes the machine configuration from the datastore
func DeleteConfigs(ids []string) error {

	var objs []Object
	for _, id := range ids {
		objs = append(objs, &api.MachineConfig{Id: id})
	}

	return DeleteObjects(objs)

}

// ListConfigs retrieves all machine configurations from the datastore
func ListConfigs() (map[string]*api.Config, error) {

	result := make(map[string]*api.Config)
	err := withEtcd(func(c *etcd.Client) error {

		ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
		resp, err := c.Get(ctx, api.MachineConfigKeyPrefix, etcd.WithPrefix())
		cancel()
		if err != nil {
			return err
		}

		for _, kv := range resp.Kvs {

			mc := &api.MachineConfig{}
			err := json.Unmarshal(kv.Value, mc)
			if err != nil {
				return err
			}
			result[mc.Id] = mc.Config

		}

		return nil

	})

	return result, err

}
