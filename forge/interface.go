package forge

import (
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"strings"
	"text/template"

	log "github.com/sirupsen/logrus"

	api "gitlab.com/mergetb/tech/foundry/api"
)

const primaryInterfaceTemplate = `
[Match]
Name={{ .Name }}

[Network]
DHCP=yes
LLMNR=no
DNSSEC=no
LLDP=yes
EmitLLDP=yes

[DHCP]
ClientIdentifier=mac
UseDomains=yes

[Route]
Gateway=172.30.0.1
Destination=192.168.254.0/24
GatewayOnlink=yes
`

type InterfaceTemplateArgs struct {
	Name string
}

func ForgePrimaryInterface() error {

	cmdline, err := ioutil.ReadFile("/proc/cmdline")
	if err != nil {
		return fmt.Errorf("failed to read /proc/cmdline: %v", err)
	}

	args := strings.Fields(string(cmdline))
	for _, x := range args {

		parts := strings.Split(x, "=")
		if len(parts) == 2 {
			if parts[0] == "infranet" {

				return forgePrimaryInterface(parts[1])

			}
		}

	}

	return fmt.Errorf(
		"primary interface not found infranet=<ifx> expected on kernel cmdline")

}

func forgePrimaryInterface(name string) error {

	log.WithFields(log.Fields{
		"ifx": name,
	}).Info("forging primary interface")

	tmpl, err := template.New("ifx").Parse(primaryInterfaceTemplate)
	if err != nil {
		return fmt.Errorf("failed to parse template: %v", err)
	}

	filename := fmt.Sprintf("/etc/systemd/network/%s.network", name)
	f, err := os.Create(filename)
	if err != nil {
		return fmt.Errorf("failed to open %s: %v", filename, err)
	}

	err = tmpl.Execute(f, InterfaceTemplateArgs{
		Name: name,
	})

	if err != nil {
		return fmt.Errorf(
			"failed to execute primary interface template: %v", err)
	}

	return nil

}

func ForgeInterfaces(interfaces []*api.Interface) error {

	for _, ifx := range interfaces {

		if ifx.Vxlan != nil {
			err := ForgeVxlan(ifx)
			if err != nil {
				return err
			}
		} else if ifx.Vlan != nil {
			err := ForgeVlan(ifx)
			if err != nil {
				return err
			}
		} else {
			err := ForgePhy(ifx)
			if err != nil {
				return err
			}
		}
	}

	return nil

}

func ForgeVlan(ifx *api.Interface) error {

	fields := log.Fields{
		"name":  ifx.Name,
		"addrs": fmt.Sprintf("%+v", ifx.Addrs),
		"mac":   ifx.Mac,
		"mtu":   ifx.Mtu,
		"vid":   ifx.Vlan.Vid,
		"dhcp":  ifx.Vlan.Dhcp,
	}
	log.WithFields(fields).Info("add vlan")

	// read in existing network file if it exists
	cfg, err := Read(fmt.Sprintf("%s.network", ifx.Name))
	if err != nil {
		cfg = make(SystemdConfig)
		cfg.MatchName(ifx.Name)
	}
	cfg.SetVlan(ifx.Name, int(ifx.Vlan.Vid))
	cfg.Write(ifx.Name, "network")

	cfg = make(SystemdConfig)
	netdev := fmt.Sprintf("%s.%d", ifx.Name, int(ifx.Vlan.Vid))
	cfg.Netdev(netdev, "vlan")
	cfg.NetdevVlan(int(ifx.Vlan.Vid))
	cfg.Write(netdev, "netdev")

	cfg = make(SystemdConfig)
	cfg.MatchName(netdev)

	for _, addr := range ifx.Addrs {
		if ValidateAddress(addr) {
			cfg.SetAddress(addr)
		}
	}

	if ifx.Vlan.Dhcp != "" {
		if ValidateDHCP(ifx.Vlan.Dhcp) {
			cfg.SetDHCP(ifx.Vlan.Dhcp)
		} else {
			log.Errorf("not valid dhcp option: %s", ifx.Vlan.Dhcp)
			log.WithError(
				fmt.Errorf("Invalid dchp option"),
			).WithFields(log.Fields{
				"dhcp": ifx.Vlan.Dhcp,
			})
		}
	}
	cfg.Write(netdev, "network")

	return nil

}

//TODO
func ForgeVxlan(ifx *api.Interface) error {

	return nil

}

func ForgePhy(ifx *api.Interface) error {

	fields := log.Fields{
		"name":  ifx.Name,
		"addrs": fmt.Sprintf("%+v", ifx.Addrs),
		"mac":   ifx.Mac,
		"mtu":   ifx.Mtu,
		"dhcp":  ifx.Dhcp,
	}
	log.WithFields(fields).Info("add phy")

	cfg := make(SystemdConfig)
	cfg.MatchName(ifx.Name)

	for _, addr := range ifx.Addrs {
		if ValidateAddress(addr) {
			cfg.SetAddress(addr)
		}
	}

	if ifx.Dhcp != "" {
		if ValidateDHCP(ifx.Dhcp) {
			cfg.SetDHCP(ifx.Dhcp)
		} else {
			log.WithError(
				fmt.Errorf("Invalid dchp option"),
			).WithFields(log.Fields{
				"dhcp": ifx.Dhcp,
			})
		}
	}

	if int(ifx.Mtu) != 0 {
		cfg.SetMtu(int(ifx.Mtu))
	}

	//TODO mac

	return cfg.Write(ifx.Name, "network")

}

func ValidateDHCP(dhcp string) bool {
	lower := strings.ToLower(dhcp)
	if lower == "yes" || lower == "no" || lower == "ipv4" || lower == "ipv6" {
		return true
	}
	return false
}

func ValidateAddress(addr string) bool {

	if addr == "" {
		return false
	}

	_, _, err := net.ParseCIDR(addr)
	if err != nil {

		log.WithError(err).WithFields(log.Fields{
			"address": addr,
		}).Warn("invalid address")

		return false

	}

	return true

}
