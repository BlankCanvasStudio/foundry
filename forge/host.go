package forge

import (
	"os/exec"

	log "github.com/sirupsen/logrus"
)

func ForgeHostname(hostname string) error {

	log.WithFields(log.Fields{"hostname": hostname}).Info("setting hostname")

	cmd := exec.Command("hostnamectl", "set-hostname", hostname)
	out, err := cmd.CombinedOutput()
	if err != nil {
		return ErrorEF("failed to set hostname", err, log.Fields{
			"hostname": hostname,
			"output":   string(out),
		})
	}

	return nil

}
