package forge

import (
	"log"

	"github.com/mergetb/embiggen-disk"
)

func ForgeRootfs() error {

	log.Printf("expanding rootfs")

	return embiggen.Embiggen("/")

}
