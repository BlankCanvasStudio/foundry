package forge

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"os/user"
	"strconv"

	log "github.com/sirupsen/logrus"
	"github.com/tredoe/osutil/user/crypt/sha512_crypt"

	api "gitlab.com/mergetb/tech/foundry/api"
)

func ForgeUsers(users []*api.User) error {

	for _, user := range users {

		if user.Name == "" {
			return Error("username cannot be empty")
		}

		err := AddUser(user.Name, user.Password)
		if err != nil {
			return err
		}

		if len(user.SshAuthorizedKeys) > 0 {
			AddSshAuthorizedKeys(user.Name, user.SshAuthorizedKeys)
		}

		for _, group := range user.Groups {
			AddUserToGroup(user.Name, group)
		}

	}

	return nil

}

func AddUser(name, passwd string) error {

	fields := log.Fields{"name": name}
	log.WithFields(fields).Info("add user")

	// if user already exists, nothing to do
	_, err := user.Lookup(name)
	if err == nil {
		return nil
	}

	var cmd *exec.Cmd

	// check if the users own group already exists
	_, err = user.LookupGroup(name)
	if err != nil {

		if _, ok := err.(user.UnknownGroupError); ok {
			cmd = exec.Command(
				"useradd",
				"-m",              //create home directory
				"-s", "/bin/bash", // no one wants /bin/sh
				"-U", //create group with same name as user
			)
		} else {
			return ErrorEF("failed to query group", err, fields)
		}

	} else {

		cmd = exec.Command(
			"useradd",
			"-m",
			"-s", "/bin/bash",
			"-g", name,
		)

	}

	if passwd != "" {

		crypt := sha512_crypt.New()

		hash, err := crypt.Generate([]byte(passwd), nil)
		if err != nil {
			return ErrorEF("password hashing failure", err, fields)
		}
		cmd.Args = append(cmd.Args, "-p", string(hash))
	}
	cmd.Args = append(cmd.Args, name)
	out, err := cmd.CombinedOutput()
	if err != nil {
		fields["out"] = string(out)
		return ErrorEF("failed to add user", err, fields)
	}

	// delete the users password so they can set it if desired
	if passwd == "" {
		cmd = exec.Command("passwd", "-d", name)
		out, err = cmd.CombinedOutput()
		if err != nil {
			fields["out"] = string(out)
			return ErrorEF("failed to add user", err, fields)
		}
	}

	return nil

}

func AddSshAuthorizedKeys(user string, keys []string) error {

	u, err := GetPosixUser(user)
	if err != nil {
		return err
	}

	fields := log.Fields{"user": user}
	sshdir := fmt.Sprintf("/home/%s/.ssh", user)
	authfile := fmt.Sprintf("%s/authorized_keys", sshdir)
	fields["path"] = sshdir
	fields["authfile"] = authfile

	log.WithFields(fields).Info("add-ssh-key")

	// ensure the users .ssh dir exists
	err = os.MkdirAll(sshdir, 0700)
	if err != nil {
		return ErrorEF("failed to ensure users .ssh dir", err, fields)
	}
	// ensure ownership
	err = os.Chown(sshdir, u.Uid, u.Gid)
	if err != nil {
		return ErrorEF("failed to own authorized_keys file", err, fields)
	}

	// open the auth'd keys file
	f, err := os.OpenFile(authfile, os.O_CREATE|os.O_RDONLY, 0600)
	if err != nil {
		return ErrorEF("failed to open users authorized_keys", err, fields)
	}

	// keep a de-duplicating list of keys
	km := make(map[string]byte)

	// scan the contents and put into map
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		km[scanner.Text()] = 0
	}
	f.Close()
	err = scanner.Err()
	if err != nil {
		return ErrorEF("failed to read authorized_keys", err, fields)
	}

	// integrate the provided keys
	for _, key := range keys {
		km[key] = 0
	}

	// fabricate result
	var result string
	for key, _ := range km {
		result += fmt.Sprintln(key)
	}

	// write back to file
	err = ioutil.WriteFile(authfile, []byte(result), 0600)
	if err != nil {
		return ErrorEF("failed to write update authorized_keys file", err, fields)
	}

	// ensure ownership
	err = os.Chown(authfile, u.Uid, u.Gid)
	if err != nil {
		return ErrorEF("failed to own authorized_keys file", err, fields)
	}

	return nil

}

type PosixUser struct {
	Uid      int
	Gid      int
	Username string
	Name     string
	HomeDir  string
}

func GetPosixUser(name string) (*PosixUser, error) {

	fields := log.Fields{"user": name}

	u, err := user.Lookup(name)
	if err != nil {
		return nil, ErrorEF("could not lookup user", err, fields)
	}

	uid, err := strconv.Atoi(u.Uid)
	if err != nil {
		return nil, ErrorEF("failed to convert uid", err, fields)
	}

	gid, err := strconv.Atoi(u.Gid)
	if err != nil {
		return nil, ErrorEF("failed to convert gid", err, fields)
	}

	return &PosixUser{
		Uid:      uid,
		Gid:      gid,
		Username: name,
		Name:     u.Name,
		HomeDir:  u.HomeDir,
	}, nil

}

func AddUserToGroup(name, group string) error {

	fields := log.Fields{
		"name":  name,
		"group": group,
	}
	log.WithFields(fields).Info("add user to group")

	// ensure the group exists
	_, err := user.LookupGroup(group)
	if err != nil {
		err = AddGroup(group)
		if err != nil {
			return err
		}
	}

	cmd := exec.Command("usermod", "-aG", group, name)
	out, err := cmd.CombinedOutput()

	if err != nil {
		fields["out"] = string(out)
		return ErrorEF("failed to add user to group", err, fields)
	}

	return nil

}

func AddGroup(name string) error {

	fields := log.Fields{"name": name}
	log.WithFields(fields).Info("add group")

	cmd := exec.Command("groupadd", name)
	out, err := cmd.CombinedOutput()

	if err != nil {
		fields["out"] = string(out)
		return ErrorEF("failed to add user to group", err, fields)
	}

	return nil

}
