package forge

import (
	"gitlab.com/mergetb/tech/rtnl"
)

func GetIds() ([]string, error) {

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return nil, err
	}
	defer ctx.Close()

	links, err := rtnl.ReadLinks(ctx, nil)
	if err != nil {
		return nil, err
	}

	var result []string
	for _, link := range links {
		if link.Info.Type() == rtnl.PhysicalType {
			result = append(result, link.Info.Address.String())
		}
	}

	return result, nil

}
