package forge

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
)

func TestSystemdR(t *testing.T) {

	content := `[Hello]
a=b
b=123d
c=d

[HowAre]
foo=bar

[YouToday]
foo=blarg
bar=nope

[Hello]
a=asdasdasd
b=asldkasndasd
`
	tmpfile, err := ioutil.TempFile("/etc/systemd/network/", "systemdtestfile")
	if err != nil {
		t.Fatal(err)
	}
	name := tmpfile.Name()
	defer os.Remove(name)

	t.Logf("tmpfile: %s", name)

	if _, err = tmpfile.Write([]byte(content)); err != nil {
		t.Fatal(err)
	}
	if err := tmpfile.Close(); err != nil {
		t.Fatal(err)
	}

	sd, err := Read(filepath.Base(name))
	if err != nil {
		t.Fatal(err)
	}

	sections := []string{"Hello", "HowAre", "YouToday"}
	for _, s := range sections {
		if _, ok := sd[s]; !ok {
			t.Fatalf("Missing section: %s", s)
		}
	}

	if len(sd["Hello"]) != 2 {
		t.Fatal("Duplicate section read fail")
	}
}

func TestSystemdMutliSections(t *testing.T) {

	cfg := make(SystemdConfig)

	cfg.PushSection("Route")
	cfg.SetEntry("Route", "Destination", "10.0.5.1/32")
	cfg.SetEntry("Route", "Gateway", "10.0.1.1")

	cfg.PushSection("Route")
	cfg.SetEntry("Route", "Destination", "10.0.10.1/32")
	cfg.SetEntry("Route", "Gateway", "10.0.1.1")

	if _, ok := cfg["Route"]; !ok {
		t.Fatal("Missing section")
	}

	if len(cfg["Route"]) != 2 {
		t.Fatal("Wrong number of sections")
	}
}

func TestSystemdW(t *testing.T) {

	cfg := make(SystemdConfig)

	cfg.PushSection("Route")
	cfg.SetEntry("Route", "Destination", "10.0.5.1/32")
	cfg.SetEntry("Route", "Gateway", "10.0.1.1")

	cfg.PushSection("Route")
	cfg.SetEntry("Route", "Destination", "10.0.10.1/32")
	cfg.SetEntry("Route", "Gateway", "10.0.1.1")

	name := "NOTANIFACE"
	kind := "network"
	path := "/etc/systemd/network/" + name + "." + kind

	cfg.Write(name, kind)
	defer os.Remove(path)

	shouldbe := `[Route]
Destination=10.0.5.1/32
Gateway=10.0.1.1

[Route]
Destination=10.0.10.1/32
Gateway=10.0.1.1
`
	contents, err := ioutil.ReadFile(path)
	if err != nil {
		t.Fatal(err)
	}

	if string(contents) != shouldbe {
		t.Fatal("Unexpected contents from Write()")
	}
}

// GTL TODO: write a read/write/read test.
