package forge

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/coreos/go-systemd/unit"
)

// SystemdConfig keeps track of the UnitSections we are writing and updating.
type SystemdConfig map[string][]*unit.UnitSection

// MatchName creates Match Name unit file entry.
func (c SystemdConfig) MatchName(name string) {
	c.SetEntry("Match", "Name", name)
}

// SetAddress creates a Network Address unit file entry.
func (c SystemdConfig) SetAddress(value string) {
	c.SetEntry("Network", "Address", value)
}

// SetDHCP configues systemd-networkd's dhcp value
func (c SystemdConfig) SetDHCP(value string) {
	c.SetEntry("Network", "DHCP", value)
}

// SetMtu creates a Link MTUBytes unit file entry.
func (c SystemdConfig) SetMtu(value int) {
	c.SetEntry("Link", "MTUBytes", fmt.Sprintf("%d", value))
}

// SetVlan creates a Network VLAN unti file entry.
func (c SystemdConfig) SetVlan(name string, vid int) {
	c.SetEntry("Network", "VLAN", fmt.Sprintf("%s.%d", name, vid))
}

// Netdev creates two unit file NetDev entries, Name and Kind.
func (c SystemdConfig) Netdev(name, kind string) {
	c.SetEntry("NetDev", "Name", name)
	c.SetEntry("NetDev", "Kind", kind)
}

// NetdevVlan creates a VLAN Id unit file entry.
func (c SystemdConfig) NetdevVlan(vid int) {
	c.SetEntry("VLAN", "Id", fmt.Sprintf("%d", vid))
}

// SetEntry appends the given name, value pair to newest
// section if it doesn't already exist.
func (c SystemdConfig) SetEntry(section, name, value string) {

	_, ok := c[section]
	if !ok {
		c.PushSection(section)
	}

	i := len(c[section]) - 1

	// Do not add if it's already there.
	for _, e := range c[section][i].Entries {
		if e.Name == name && e.Value == value {
			return
		}
	}

	c[section][i].Entries = append(
		c[section][i].Entries,
		&unit.UnitEntry{Name: name, Value: value},
	)
}

// PushSection starts a new section. Any SetEntries will be applied
// to the new section.
func (c SystemdConfig) PushSection(section string) {

	c[section] = append(
		c[section],
		&unit.UnitSection{Section: section, Entries: []*unit.UnitEntry{}},
	)
}

// Write the current unit file entries to the appropriate Network config file.
func (c SystemdConfig) Write(name, kind string) error {

	filename := fmt.Sprintf("/etc/systemd/network/%s.%s", name, kind)

	// build slice of all sections.
	sections := []*unit.UnitSection{}
	for _, s := range c {

		sections = append(sections, s...)
	}

	buf, err := ioutil.ReadAll(unit.SerializeSections(sections))
	if err != nil {
		return err
	}

	return ioutil.WriteFile(filename, buf, 0644)

}

// Read and parse the given unit file into a SystemdConfig instance.
func Read(filename string) (SystemdConfig, error) {

	f, err := os.Open(fmt.Sprintf("/etc/systemd/network/%s", filename))
	if err != nil {
		return nil, fmt.Errorf("failed to open file %s", filename)
	}

	sections, err := unit.DeserializeSections(f)
	if err != nil {
		return nil, fmt.Errorf("failed to parse systemd file %s: %v", filename, err)
	}

	// Build our mapped data from the given sections.
	c := make(SystemdConfig)
	for _, s := range sections {
		c.PushSection(s.Section)
		for _, e := range s.Entries {
			c.SetEntry(s.Section, e.Name, e.Value)
		}
	}

	return c, nil
}
