package forge

import (
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"os/exec"

	log "github.com/sirupsen/logrus"
	api "gitlab.com/mergetb/tech/foundry/api"
	"gitlab.com/mergetb/tech/rtnl"
)

const (
	WireguardSubnet = "192.168.254.0/24"

	//TODO get from foundryd
	InfranetGateway = "172.30.0.1"
)

func ForgeRoutes() error {

	_, lnk, err := getDefaultInfranetRoute()
	if err != nil {
		log.Warn(
			"no default infranet route found not setting up wireguard route")
		return nil
	}

	cfg, err := Read(fmt.Sprintf("%s.network", lnk.Info.Name))
	if err != nil {
		return err
	}

	cfg.SetEntry("Route", "Gateway", InfranetGateway)
	cfg.SetEntry("Route", "Destination", WireguardSubnet)

	//XXX workaround networkd bug
	// https://github.com/systemd/systemd/issues/1850
	cfg.SetEntry("Route", "GatewayOnlink", "yes")

	return cfg.Write(lnk.Info.Name, "network")

}

func getDefaultInfranetRoute() (*rtnl.Route, *rtnl.Link, error) {

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return nil, nil, err
	}

	routes, err := rtnl.ReadRoutes(ctx, nil)
	if err != nil {
		return nil, nil, err
	}

	for _, x := range routes {
		if x.Dest == nil && x.Gateway.String() == InfranetGateway {
			lnk, err := rtnl.GetLinkByIndex(ctx, int32(x.Oif))
			if err != nil {
				return nil, nil, err
			}
			return x, lnk, nil
		}
	}

	return nil, nil, fmt.Errorf("no default route found")

}

// getIfaceSubnetContains returns the interface name that is in the subnet of the given address.
func getIfaceSubnetContains(ip net.IP, interfaces []*api.Interface) (string, error) {

	log.Debugf("Looking for %s in %d interfaces", ip, len(interfaces))

	for _, ifx := range interfaces {
		for _, addr := range ifx.Addrs {
			_, ipn, err := net.ParseCIDR(addr)
			if err != nil {
				log.WithError(err).WithFields(log.Fields{
					"address": addr,
				}).Warn("invalid address")
				return "", fmt.Errorf("Bad CIDR on iface")
			}

			if ipn.Contains(ip) {
				// found the iface. now find the iface name.
				if ifx.Vlan == nil {
					log.Debugf("Found %s in %s", ip, ifx.Name)
					return ifx.Name, nil
				}

				name := fmt.Sprintf("%s.%d", ifx.Name, int(ifx.Vlan.Vid))
				log.Debugf("Found %s in %s", ip, name)

				return name, nil
			}
		}
	}

	return "", fmt.Errorf("no interface found for subnet %s", ip)
}

func ForgeStaticRoutes(routes []*api.Route, interfaces []*api.Interface) error {

	log.Infof("Adding %d static routes", len(routes))

	for _, r := range routes {

		log.Debugf("Adding route: %+v", r)

		_, dnet, err := net.ParseCIDR(r.Dst)
		if err != nil {
			return fmt.Errorf("bad destintation in route: %s", r.Dst)
		}

		gwip := net.ParseIP(r.Gateway)
		if gwip == nil {
			return fmt.Errorf("bad gateway in route: %s", r.Gateway)
		}

		ifname, err := getIfaceSubnetContains(gwip, interfaces)
		if err != nil {
			return fmt.Errorf(
				"Unable to find interface for gateway %s (%s): %w",
				r.Gateway, gwip, err)
		}

		log.Debugf("Found gw iface: %s", ifname)

		cfg, err := Read(fmt.Sprintf("%s.network", ifname))
		if err != nil {
			return fmt.Errorf("Read error: %w", err)
		}

		log.Tracef("Read cfg: %+v", cfg)

		// Each route is its own [Route] section.
		cfg.PushSection("Route")

		// Save into net config for perminence.
		cfg.SetEntry("Route", "Destination", dnet.String())
		cfg.SetEntry("Route", "Gateway", gwip.String())

		//XXX workaround networkd bug
		// https://github.com/systemd/systemd/issues/1850
		cfg.SetEntry("Route", "GatewayOnlink", "yes")

		log.Debugf("Pushed [Route]: %s -> %s via %s", dnet.String(), gwip.String(), ifname)

		err = cfg.Write(ifname, "network")
		if err != nil {
			return fmt.Errorf("writing %s network config: %w", ifname, err)
		}
	}

	if len(interfaces) > 1 {
		err := setIPForwarding()
		if err != nil {
			return err
		}

		err = disableReversePathFiltering(interfaces)
		if err != nil {
			return err
		}
	}

	return nil
}

func setIPForwarding() error {

	log.Info("Enabling ip forwarding")

	// strategy is to create a /etc/sysctl.d file, add our entry, and sysctl -p to reload.
	path := "/etc/sysctl.d/merge_ip_forward.conf"
	entry := []byte("net.ipv4.ip_forward=1")

	if _, err := os.Stat(path); os.IsNotExist(err) {
		err = ioutil.WriteFile(path, entry, 0644)
		if err != nil {
			return err
		}

		cmd := exec.Command("sysctl", "-p", path)
		out, err := cmd.CombinedOutput()
		if err != nil {
			return ErrorEF("applying ip forwarding", err, log.Fields{
				"out": out,
			})
		}
	}

	// if it exists, we assume we've already done our work. This could be a bad assumption.
	return nil
}

func disableReversePathFiltering(interfaces []*api.Interface) error {

	// If we don't unset reverse path filtering, the kernel will drop
	// packates that aren't from the subnet of the interface. But this
	// happens all the time when using static routing in foundry as the
	// paths that it generates are not symmetric.
	//
	// See https://www.slashroot.in/linux-kernel-rpfilter-settings-reverse-path-filtering
	// and https://tldp.org/HOWTO/Adv-Routing-HOWTO/lartc.kernel.rpf.html
	//

	log.Info("Disable reverse path forwarding on non-infranet interfaces")

	path := "/etc/sysctl.d/reverse_path_filters.conf"
	fd, err := os.Create(path)
	if err != nil {
		return err
	}
	defer fd.Close()

	for _, iface := range interfaces {

		cmd := fmt.Sprintf("net.ipv4.conf.%s.rp_filter=2", iface.Name)
		fd.WriteString(cmd + "\n")

	}

	// now apply the file we just wrote.
	err = exec.Command("sysctl", "-p").Run()
	if err != nil {
		return err
	}

	return nil
}
