package forge

import (
	"fmt"
	"io/ioutil"
	"os/exec"
	"regexp"
	"strings"
)

func ForgeCmdline() error {

	orig, err := ioutil.ReadFile("/etc/default/grub")
	if err != nil {
		return err
	}

	buf, err := ioutil.ReadFile("/proc/cmdline")
	if err != nil {
		return fmt.Errorf("failed to read /proc/cmdline: %v", err)
	}
	cmdline := strings.TrimSuffix(string(buf), "\n")

	re := regexp.MustCompile(`GRUB_CMDLINE_LINUX=".*"`)

	new := re.ReplaceAll(orig, []byte(
		fmt.Sprintf("GRUB_CMDLINE_LINUX=\"%s\"", cmdline),
	))

	if string(orig) == string(new) {
		return nil
	}

	err = ioutil.WriteFile("/etc/default/grub", new, 0644)
	if err != nil {
		return fmt.Errorf("failed to write to /etc/default/grub: %v", err)
	}

	out, err := exec.Command("update-grub2").CombinedOutput()
	if err != nil {
		return fmt.Errorf("update grub failed: %s: %v", string(out), err)
	}

	return nil

}
