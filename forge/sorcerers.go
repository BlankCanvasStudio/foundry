package forge

import (
	"fmt"
	"strings"
	"os/exec"
)

// MatchName creates Match Name unit file entry.
func StartSorcerers(names string) error {

	names = strings.ToLowerCase(names)  

	// Start all the sorcerers 
	if strings.Contains(names, "all") {
		services: = []string{ "ansible", "bash-dump", "bash-recording", "jupyter", "logs", "file", 
			"id", "id-server", "network", "os-dump", "os-recording", "validation" }
		return enableServices(services)
	}
	// Start a custom amount of sorcerers
	return enableServices(strings.Split(names, " "))
}


func enableService(services []string) error {

	for _, service := range services {

		service_name := "discern-" + service

		cmd := exec.Command("systemctl", "enable", service_name)
		out, err := cmd.CombinedOutput()

		if err != nil {
			return ErrorE(
				fmt.Sprintf("Cannot enable service: ", service_name), 
				err);
		}

	}

	return nil
}

